from django.urls import path
from . import views

app_name = 'myproject'

urlpatterns = [
    path('', views.home, name='home'),
    path('profile/', views.profile, name='profile'),
    path('about-me/', views.about, name='about'),
    path('contacts/', views.contacts, name='contacts'),
	path('events/', views.list, name='schedule'),
	path('delete/(?P<delete_id>[0-9]+)', views.delete, name='delete'),
	path('events/add-event/', views.create, name='event'),
]
